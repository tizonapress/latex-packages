\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{tznotes}[2022/10/25 v1.1 A class for study notes]
\LoadClass[11pt]{report}
\RequirePackage{titlesec}
\RequirePackage[margin=1in]{geometry}

% romanize section numbering
\renewcommand{\thesection}{\Roman{section}.}
