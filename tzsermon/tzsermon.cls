\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{tzsermon}[2022/10/25 v1.3 A class for sermon preparation]
\LoadClass[11pt]{article}
\RequirePackage{titlesec}
\RequirePackage[margin=1in]{geometry}

% directions to congregation
\newcommand{\direct}[1]{\textbf{Direct:} #1}

% notes of dates and locations where this message has been preached
\newcommand{\preached}[2]{\textbf{Preached} at #1 on #2}
\newcommand{\unpreached}[0]{\textbf{Unpreached}}

% romanize section numbering
\renewcommand{\thesection}{\Roman{section}.}

% sermon sections
\newenvironment{introduction}[1]
	{\section{\textbf{Introduction:} #1}}
	{\vspace{1ex}}

\newenvironment{setting}[1]
	{\section{\textbf{Setting:} #1}}
	{\vspace{1ex}}

\newenvironment{support}[1]
	{\section{\textbf{Support:} #1}}
	{\vspace{1ex}}

\newenvironment{application}[1]
	{\section{\textbf{Application:} #1}}
	{\vspace{1ex}}

\newenvironment{conclusion}[1]
	{\section{\textbf{Conclusion:} #1}}
	{\vspace{1ex}}
